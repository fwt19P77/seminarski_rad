'use strict';


module.exports = {
  up: function (queryInterface, Sequelize) {
    // logic for transforming into the new state
    return queryInterface.addColumn(
      "Dokumentacijas",
      'StudentId',
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'Students', // name of Target model
          key: 'id', // key in Target model that we're referencing
        }
      }
    )

  },

  down: function (queryInterface, Sequelize) {
    // logic for reverting the changes
    return queryInterface.removeColumn(
      'Dokumentacijas',
      'StudentId'
    );
  }
}