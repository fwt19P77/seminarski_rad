'use strict';
const {
  Model, Sequelize
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Dokumentacija extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {

      models.Dokumentacija.belongsTo(models.Student)

    }
  };
  Dokumentacija.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true
    },
    name: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'Dokumentacija',
  });
  return Dokumentacija;
};